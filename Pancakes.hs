felicidadAbsoluta :: [Bool] -> Int
felicidadAbsoluta xs = felicidadAbsolutaAux xs 0

felicidadAbsolutaAux :: [Bool] -> Int -> Int
felicidadAbsolutaAux xs n = if estanTodosFelices xs
                            then n
                            else felicidadAbsolutaAux (girarIguales xs) (n+1) 

estanTodosFelices :: [Bool] -> Bool
estanTodosFelices xs = foldr1 (&&) xs

girarIguales :: [Bool] -> [Bool]
girarIguales xs = let iguales = extraerPrimerosIguales xs
                  in girar iguales ++ dropN (length iguales) xs  

girar :: [Bool] -> [Bool]
girar xs = map not xs

extraerPrimerosIguales :: [Bool] -> [Bool]
extraerPrimerosIguales [] = []
extraerPrimerosIguales [x] = [x]
extraerPrimerosIguales (x:y:xs) = if x == y
                                    then x : extraerPrimerosIguales (y:xs)
                                    else x : []

dropN :: Int -> [a] -> [a]
dropN 0 xs = xs
dropN n (x:xs) = dropN (n-1) xs

